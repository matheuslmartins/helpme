<?php

namespace App\Http\Controllers;

use App\Help;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAll(){
        $helps = Help::all();

        return response()->json($helps);
    }

    public function get($id){
        $help = Help::find($id);
        
        return response()->json($help);
    }

    public function create(Request $request){
        $help = new Help($request->json()->all());
        $help->save();
        return $help;
    }

    public function update(Request $request, $id){
        $help = Help::find($id);

        $help->nome = $request->json()->get('nome');
        $help->help = $request->json()->get('help');
        $help->save();
        return response()->json($help);
    }

    public function delete($id){
        $help = Help::find($id);
        $help->delete();

        return response()->json('deletado tio');
    }

    
}
