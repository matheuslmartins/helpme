<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/helpme', 'HelpController@getAll');

$router->get('/helpme/{id}', 'HelpController@get');

$router->post('/helpme', 'HelpController@create');

$router->put('/helpme/{id}', 'HelpController@update');

$router->delete('/helpme/{id}', 'HelpController@delete');
